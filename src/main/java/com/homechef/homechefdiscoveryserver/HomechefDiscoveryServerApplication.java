package com.homechef.homechefdiscoveryserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class HomechefDiscoveryServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(HomechefDiscoveryServerApplication.class, args);
    }

}
